package es.caib.autenticacio.afirma;

import org.keycloak.authentication.authenticators.x509.AbstractX509ClientCertificateAuthenticatorFactory;
import org.keycloak.authentication.Authenticator;
import org.keycloak.models.AuthenticationExecutionModel;
import org.keycloak.models.KeycloakSession;

public class AfirmaClientCertificateAuthenticatorFactory extends AbstractX509ClientCertificateAuthenticatorFactory {
    // public static final String PROVIDER_ID = "auth-x509-client-username-form";
	public static final String PROVIDER_ID = "auth-x509-afirma";
    public static final AfirmaClientCertificateAuthenticator SINGLETON =
            new AfirmaClientCertificateAuthenticator();

    public static final AuthenticationExecutionModel.Requirement[] REQUIREMENT_CHOICES = {
            AuthenticationExecutionModel.Requirement.ALTERNATIVE,
            AuthenticationExecutionModel.Requirement.DISABLED
    };


    @Override
    public String getHelpText() {
        return "Valida el certificat contra @firma";
    }

    @Override
    public String getDisplayType() {
        return "X509@firma";
    }

    @Override
    public AuthenticationExecutionModel.Requirement[] getRequirementChoices() {
        return REQUIREMENT_CHOICES;
    }


    @Override
    public Authenticator create(KeycloakSession session) {
        return SINGLETON;
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }
}
